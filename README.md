# Requirements

- Xcode 10.3
- Swift 5

# Frameworks

| Framework                                                                                                                   | Usage                                                           | Detail                                                                                                                                                                                          |
| --------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [RxSwift](https://github.com/ReactiveX/RxSwift)                                                                             | Swift version of Rx                                             |
| [RealmSwift](https://github.com/realm/realm-cocoa) <br/> [RxRealm](https://github.com/RxSwiftCommunity/RxRealm)             | Mobile database                                                 | 1. Easy set up <br/> 2. Better support Rx <br/> 3. Cross platform support(Such as Android) <br/> 4. Faster performance <br/> 5. Easy encrpytion <br/> 6. Free <br/> 7. No limit to data storing |
| [Swinject](https://github.com/Swinject/Swinject) <br/> [SwinjectStoryboard](https://github.com/Swinject/SwinjectStoryboard) | Lightweight **Dependency Injection (DI)** framework for Swift.  | Implements **Inversion of Control (IoC)** for resolving dependencies. Swinject helps your app split into loosely-coupled components, which can be developed, tested and maintained more easily. |
| [SwiftGen](https://github.com/SwiftGen/SwiftGen)                                                                            | Tool to auto-generate Swift code for resources of your projects | 1. Assets <br/> 2. Storyboards                                                                                                                                                                  |
| [Alamofire](https://github.com/Alamofire/Alamofire)                                                                         | HTTP networking library                                         |
| [SDWebImage](https://github.com/SDWebImage/SDWebImage)                                                                      | Async image downloader with cache support                       |                                                                                                                                                                                                 |

# Code Structure

**Model-View-ViewModel (_MVVM_)** Design Pattern

Client persists data on Realm Database.

UI (Collection View and Table View) is binding to a collection of Realm Change Sets, that mean, UI will perform batch updates on secific change sets (insert, delete, modifications) instead of reload whole view frequently.

Client will send request only once when database does't have the data.

## Model

`Episode`, `Show`, `Cast` are models used in UI layer.

`EpisodeRM`, `ShowRM`, `CastRM` are realm object, which are strictly used in database layer.

## ViewModel

Contains series of observables, which are ready to display, or only need simple compute logic.

No heavy business logic.

## Controller and View

Layout views if necessary, bind to display series of observables.

Should not contains any business logic.

## Interactor

Process specific business rule (or single user case)

Send request to server.

## Adapter

Wrapper classes for realm, which can be reused.

## Others

- Use storyboard **Auto Layout**, reduce code lines
- Use **Collection View** in show list, and **Table View** for detail

#

![iPhone](./readme/iPhone.gif)

![iPad](./readme/iPad.gif)
