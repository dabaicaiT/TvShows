//
//  TvShowsUITests.swift
//  TvShowsUITests
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows

class TvShowsUITests: XCTestCase {
    
    private let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        app.launch()
    }
    
    func testEnterDetail() {
        app.buttons.element(boundBy: 2).tap()
        app.collectionViews.cells.element(boundBy: 0).tap()
        
        XCTAssert(app.staticTexts["Casting:"].exists)
        
    }
    
}
