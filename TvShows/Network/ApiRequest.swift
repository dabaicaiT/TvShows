//
//  ApiRequest.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import Alamofire

protocol ApiRequest {
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var parameters: [String: Any]? { get }
    var headers: [String: String]? { get }
}

struct ApiConfig {
    static let baseUrl: String = "https://api.tvmaze.com"
    static let defaultCountryCode: String = "US"
}
