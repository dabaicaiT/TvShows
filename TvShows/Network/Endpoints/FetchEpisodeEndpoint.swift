//
//  FetchScheduleByCountryAndDateEndpoint.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Alamofire

final class FetchEpisodeEndpoint {
    
    struct Request: ApiRequest {
        let path = "/schedule"
        let httpMethod = HTTPMethod.get
        let parameters: [String: Any]?
        let headers: [String: String]? = nil
        
        
        init(date: String) {
            self.parameters = ["country": ApiConfig.defaultCountryCode,
                               "date": date]
        }
    }
    
}
