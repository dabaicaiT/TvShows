//
//  FetchCastEndpoint.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Alamofire

final class FetchCastEndpoint {
    
    struct Request: ApiRequest {
        let path: String
        let httpMethod = HTTPMethod.get
        let parameters: [String: Any]? = nil
        let headers: [String: String]? = nil
        
        
        init(id: Int) {
            self.path = "/shows/\(id.description)/cast"
        }
    }
    
}
