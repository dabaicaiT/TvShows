//
//  NetworkingAssembly.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Swinject
import Alamofire

struct NetworkingAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(ApiService.self) { r in
            let configuration = URLSessionConfiguration.default
            let sessionManager = Alamofire.SessionManager(configuration: configuration)
            return ApiServiceImpl(sessionManager: sessionManager)
        }
        .inObjectScope(.container)
        
        container.register(TvMazeGateway.self) { r in
            return TvMazeGatewayImpl(apiService: r.resolve(ApiService.self)!)
        }
    }
    
}
