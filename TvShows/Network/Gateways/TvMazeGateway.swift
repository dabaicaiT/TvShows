//
//  TvMazeGateway.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxSwift

protocol TvMazeGateway: class {
    func fetchEpisode(by date: String) -> Observable<ApiServiceResult<[Episode]>>
    func fetchCast(by showId: Int) -> Observable<ApiServiceResult<[Cast]>>
}

final class TvMazeGatewayImpl: TvMazeGateway {
    
    private let apiService: ApiService!
    
    init(apiService: ApiService) {
        self.apiService = apiService
    }
    
    func fetchEpisode(by date: String) -> Observable<ApiServiceResult<[Episode]>> {
        return apiService.perform(request: FetchEpisodeEndpoint.Request(date: date))
    }
    
    func fetchCast(by showId: Int) -> Observable<ApiServiceResult<[Cast]>> {
        return apiService.perform(request: FetchCastEndpoint.Request(id: showId))
    }
    
}
