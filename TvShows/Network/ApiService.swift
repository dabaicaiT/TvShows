//
//  ApiService.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

enum ApiServiceResult<T> {
    case success(T)
    case error(Error)
}

enum ApiServiceError: Error {
    case invalidUrl
    case invalidResponse
}

protocol ApiService: class {
    func perform<Req: ApiRequest, Resp: Decodable>(request: Req) -> Observable<ApiServiceResult<Resp>>
}

final class ApiServiceImpl: ApiService {
    
    private let sessionManager: SessionManager!
    
    init(sessionManager: SessionManager) {
        self.sessionManager = sessionManager
    }
    
    func perform<Req: ApiRequest, Resp: Decodable>(request: Req) -> Observable<ApiServiceResult<Resp>> {
        return Observable.create { [weak self] observer in
            guard let `self` = self else {
                return Disposables.create()
            }
            
            guard let url = self.getUrl(request: request) else {
                observer.onNext(.error(ApiServiceError.invalidUrl))
                observer.onCompleted()
                return Disposables.create()
            }
            
            self.sessionManager.request(url,
                                        method: request.httpMethod,
                                        parameters: request.parameters,
                                        headers: request.headers)
                .responseData { response in
                    switch response.result {
                    case .success:
                        do {
                            guard let data = response.data else {
                                observer.onNext(.error(ApiServiceError.invalidResponse))
                                observer.onCompleted()
                                return
                            }
                            
                            let genericModel = try JSONDecoder().decode(Resp.self, from: data)
                            observer.onNext(.success(genericModel))
                            observer.onCompleted()
                        } catch let error {
                            observer.onNext(.error(error))
                            observer.onCompleted()
                        }
                    case .failure(let error):
                        observer.onNext(.error(error))
                        observer.onCompleted()
                    }
            }
            return Disposables.create()
        }
    }
    
    //
    private func getUrl<T: ApiRequest>(request: T) -> URL? {
        return URL(string: ApiConfig.baseUrl + request.path)
    }
    
}
