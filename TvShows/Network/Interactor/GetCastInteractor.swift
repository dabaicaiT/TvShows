//
//  GetCastInteractor.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxSwift

protocol GetCastInteractor: class {
    func execute(showId: Int?) -> Observable<[Cast]?>
}

final class GetCastInteractorImpl: GetCastInteractor {
    
    private let tvMazeGateway: TvMazeGateway!
    
    init(tvMazeGateway: TvMazeGateway) {
        self.tvMazeGateway = tvMazeGateway
    }
    
    func execute(showId: Int?) -> Observable<[Cast]?> {
        guard let showId = showId else {
            return .just(nil)
        }
        
        return tvMazeGateway.fetchCast(by: showId)
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .map {
                switch $0 {
                case .success(let casts):
                    return casts
                case .error(let error):
                    print("[Error] \(error.localizedDescription)")
                    return nil
                }
            }
            .startWith(nil)
    }
    
}
