//
//  AppServiceAssembly.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Swinject
import Alamofire

struct InteractorAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(GetEpisodeInteractor.self) { r in
            return GetEpisodeInteractorImpl(tvMazeGateway: r.resolve(TvMazeGateway.self)!)
        }
        
        container.register(GetCastInteractor.self) { r in
            return GetCastInteractorImpl(tvMazeGateway: r.resolve(TvMazeGateway.self)!)
        }
    }
    
}
