//
//  GetEpisodesInteractor.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxSwift

protocol GetEpisodeInteractor: class {
    func execute(date: String) -> Observable<[Episode]?>
}

final class GetEpisodeInteractorImpl: GetEpisodeInteractor {
    
    private let tvMazeGateway: TvMazeGateway!
    
    init(tvMazeGateway: TvMazeGateway) {
        self.tvMazeGateway = tvMazeGateway
    }
    
    func execute(date: String) -> Observable<[Episode]?> {
        guard date.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}") else {
            return .just(nil)
        }
        
        return tvMazeGateway.fetchEpisode(by: date)
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .map {
                switch $0 {
                case .success(let episodes):
                    return episodes
                case .error(let error):
                    print("[Error] \(error.localizedDescription)")
                    return nil
                }
            }
            .startWith(nil)
    }
    
}
