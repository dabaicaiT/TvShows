//
//  ShowsCollectionViewModel.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol EpisodeListViewModelInput {
    var itemSelected: PublishSubject<Int> { get }
    
    func didLoad()
}

protocol EpisodesListViewModelOutput {
    var isLoading: Driver<Bool> { get }
    var itemsCount: Int { get }
    var changeset: BehaviorRelay<Changeset> { get }
    var enterDetail: PublishSubject<Episode> { get }
    
    func item(at index: Int) -> EpisodeCollectionCellViewModel?
}

final class EpisodeListViewModel: EpisodeListViewModelInput, EpisodesListViewModelOutput {
    
    // input
    let itemSelected = PublishSubject<Int>()
    
    // output
    let isLoading: Driver<Bool>
    var itemsCount: Int {
        return items.value.count
    }
    let changeset = BehaviorRelay(value: Changeset.empty())
    let enterDetail = PublishSubject<Episode>()
    
    private let items = BehaviorRelay(value: [EpisodeCollectionCellViewModel]())
    private let episodeRepository: EpisodeRepository!
    private let tabContext: HomeViewController.TabContext!
    private let disposeBag = DisposeBag()
    
    init(episodeRepository: EpisodeRepository,
         tabContext: HomeViewController.TabContext) {
        self.episodeRepository = episodeRepository
        self.tabContext = tabContext
        
        self.isLoading = episodeRepository.isLoading.asDriver(onErrorJustReturn: false)
        
        itemSelected
            .compactMap { self.item(at: $0)?.episode }
            .bind(to: enterDetail)
            .disposed(by: disposeBag)
    }
    
    // input
    func didLoad() {
        observeChanges()
    }
    
    // output
    func item(at index: Int) -> EpisodeCollectionCellViewModel? {
        guard index < itemsCount else {
            return nil
        }
        return items.value[index]
    }
    
    //
    private func observeChanges() {
        episodeRepository.getChanges(by: tabContext.date, filter: tabContext.filter)
            .subscribe(onNext: { [unowned self] changes in
                self.emit(changes: changes)
            })
            .disposed(by: disposeBag)
    }
    
    private func emit(changes: EpisodeChanges) {
        items.accept(changes.items.map {
            EpisodeCollectionCellViewModel(episode: $0)
        })
        changeset.accept(changes.changeset ?? Changeset.empty())
    }
    
}
