//
//  ShowDetailViewController.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ShowDetailViewController: UIViewController {
    
    @IBOutlet weak var castTableView: UITableView!
    
    var viewModel: ViewModel<ShowDetailViewModelInput, ShowDetailViewModelOutput>!
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupOutput()
        
        viewModel.input.didLoad()
    }
    
    //
    private func setupViews() {
        navigationController?.navigationBar.tintColor = .black
        
        let cellNibb = UINib(nibName: CastTableViewCell.nibName,
                             bundle: Bundle.main)
        castTableView.register(cellNibb,
                               forCellReuseIdentifier: CastTableViewCell.identifier)
        
        castTableView.rowHeight = 90
        castTableView.tableFooterView = UIView()
    }
    
    private func setupOutput() {
        viewModel.output.changeset
            .asDriver()
            .drive(onNext: { [weak self] changes in
                self?.refreshTableView(with: changes, in: 0)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.headerViewModel
            .subscribe(onNext: { [unowned self] model in
                self.configureHeader(model)
            })
            .disposed(by: disposeBag)
    }
    
    private func configureHeader(_ model: ShowDetailHeaderViewModel) {
        let height = model.headerHeight
        let width = castTableView.frame.width
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        let view = ShowDetailHeaderView(frame: frame)
        view.set(viewModel: model)
        castTableView.tableHeaderView = view
    }
    
    private func refreshTableView(with changeset: Changeset, in section: Int) {
        if changeset.isEmpty {
            castTableView.reloadData()
            return
        }
        
        let indexPath: (Int) -> IndexPath = { IndexPath(row: $0, section: section) }
        
        castTableView.beginUpdates()
        castTableView.insertRows(at: changeset.insertions.map(indexPath), with: .automatic)
        castTableView.deleteRows(at: changeset.deletions.map(indexPath), with: .automatic)
        castTableView.reloadRows(at: changeset.modifications.map(indexPath), with: .none)
        castTableView.endUpdates()
    }
    
}

extension ShowDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.output.itemsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: CastTableViewCell.identifier,
                                 for: indexPath) as! CastTableViewCell
        let cast = viewModel.output.item(at: indexPath.row)
        cast.map(cell.set(viewModel:))
        return cell
    }
    
}
