//
//  ShowDetailHeaderViewModel.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class ShowDetailHeaderViewModel {
    
    var seasonNumber: Driver<String> {
        return getSeasonNumber()
    }
    
    var summary: Driver<String> {
        return getSummary()
    }
    
    var genres: Driver<String> {
        return getGenres()
    }
    
    var duration: Driver<String> {
        return getDuration()
    }
    
    var posterUrl: URL? {
        return getPosterUrl()
    }
    
    var headerHeight: CGFloat {
        return getHeaderHeight()
    }
    
    private let episode: Episode!
    
    init(episode: Episode) {
        self.episode = episode
    }
    
    private func getSeasonNumber() -> Driver<String> {
        let seasonNumber = "\(episode.season?.description ?? "0") x \(episode.number?.description ?? "0")"
        return Observable
            .just(seasonNumber)
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getSummary() -> Driver<String> {
        return Observable
            .just(episode.summary?.plainTextFromHTML() ?? "")
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getGenres() -> Driver<String> {
        let genresList = episode.show?.genres?.joined(separator: " | ")
        return Observable
            .just("\(genresList ?? "")")
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getDuration() -> Driver<String> {
        let runtime = episode.show?.runtime
        let duration = runtime == nil ? "unknown" : "\(runtime!.description)"
        return Observable
            .just(duration)
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getPosterUrl() -> URL? {
        let urlString = episode.show?.image?.original
        return urlString == nil ? nil : URL(string: urlString!)
    }
    
    private func getHeaderHeight() -> CGFloat {
        let height = episode.summary?.height(constrained: UIScreen.main.bounds.width - 40,
                                             font: UIFont.systemFont(ofSize: 17))
        return 410 + (height ?? 0)
    }
    
}
