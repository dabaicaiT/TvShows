//
//  ShowDetailHeaderView.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable
final class ShowDetailHeaderView: UIView {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var episodeLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    private let nibName = String(describing: ShowDetailHeaderView.self)
    
    private let disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        commonInit()
    }
    
    func set(viewModel: ShowDetailHeaderViewModel) {
        posterImage.load(from: viewModel.posterUrl,
                         with: "placeholder_original")
        
        viewModel.seasonNumber
            .drive(episodeLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.summary
            .drive(summaryLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.genres
            .drive(genreLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.duration
            .drive(durationLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    //
    private func commonInit() {
        let nib = UINib(nibName: nibName, bundle: Bundle.main)
        guard let view = nib.instantiate(withOwner: self).first as? UIView else {
            return
        }
        
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
}
