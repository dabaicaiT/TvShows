//
//  CastTableViewCell.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class CastTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: CastTableViewCell.self)
    static let nibName = String(describing: CastTableViewCell.self)
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var personNameLabel: UILabel!
    @IBOutlet weak var asLabel: UILabel!
    @IBOutlet weak var characterNameLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        resetViews()
    }
    
    override func prepareForReuse() {
        resetViews()
        
        super.prepareForReuse()
    }
    
    func set(viewModel: CastTableCellViewModel) {
        avatarImage.load(from: viewModel.avatarUrl,
                         with: "avatar")
        
        viewModel.personName
            .drive(personNameLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.characterName
            .drive(characterNameLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.isSelf
            .drive(asLabel.rx.isHidden)
            .disposed(by: disposeBag)
        
        viewModel.isSelf
            .drive(characterNameLabel.rx.isHidden)
            .disposed(by: disposeBag)
    }
    
    //
    private func resetViews() {
        avatarImage.image = nil
        personNameLabel.text = nil
        characterNameLabel.text = nil
    }
    
}
