//
//  CastTableCellViewModel.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class CastTableCellViewModel {
    
    var personName: Driver<String> {
        return getPersonName()
    }
    
    var characterName: Driver<String?> {
        return getCharacterName()
    }
    
    var isSelf: Driver<Bool> {
        return getIsSelf()
    }
    
    var avatarUrl: URL? {
        return getAvatarUrl()
    }
    
    private let cast: Cast!
    
    init(cast: Cast) {
        self.cast = cast
    }
    
    //
    private func getPersonName() -> Driver<String> {
        return Observable
            .just(cast.person?.name ?? "")
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getCharacterName() -> Driver<String?> {
        return Observable
            .just(cast.character?.name)
            .asDriver(onErrorJustReturn: nil)
    }
    
    private func getIsSelf() -> Driver<Bool> {
        return Observable
            .just(cast.isSelf)
            .asDriver(onErrorJustReturn: false)
    }
    
    private func getAvatarUrl() -> URL? {
        let urlString = cast.person?.image?.medium
        return urlString == nil ? nil : URL(string: urlString!)
    }
    
}
