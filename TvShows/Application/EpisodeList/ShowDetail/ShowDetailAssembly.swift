//
//  ShowDetailAssembly.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Swinject

struct ShowDetailAssembly: Assembly {
    
    func assemble(container: Container) {
        container.storyboardInitCompleted(ShowDetailViewController.self) { (r, c, data: Episode) in
            let viewModel = r.resolve(ShowDetailViewModel.self, argument: data)!
            c.viewModel = ViewModel(viewModel, viewModel)
        }
        
        container.register(ShowDetailViewModel.self) { (r, episode: Episode) in
            return ShowDetailViewModel(
                castRepository: r.resolve(CastRepository.self)!,
                episode: episode
            )
        }
    }
    
}
