//
//  ShowDetailViewModel.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ShowDetailViewModelInput {
    func didLoad()
}

protocol ShowDetailViewModelOutput {
    var itemsCount: Int { get }
    var changeset: BehaviorRelay<Changeset> { get }
    var headerViewModel: PublishSubject<ShowDetailHeaderViewModel> { get }
    
    func item(at index: Int) -> CastTableCellViewModel?
}

final class ShowDetailViewModel: ShowDetailViewModelInput, ShowDetailViewModelOutput {
    
    // input
    let itemSelected = PublishSubject<Int>()
    
    // output
    var itemsCount: Int {
        return items.value.count
    }
    let changeset = BehaviorRelay(value: Changeset.empty())
    let headerViewModel = PublishSubject<ShowDetailHeaderViewModel>()
    
    private let items = BehaviorRelay(value: [CastTableCellViewModel]())
    private let castRepository: CastRepository!
    private let episode: Episode!
    private let disposeBag = DisposeBag()
    
    init(castRepository: CastRepository!,
         episode: Episode) {
        self.castRepository = castRepository
        self.episode = episode
    }
    
    // input
    func didLoad() {
        headerViewModel.onNext(ShowDetailHeaderViewModel(episode: episode))
        observeChanges()
    }
    
    // output
    func item(at index: Int) -> CastTableCellViewModel? {
        guard index < itemsCount else {
            return nil
        }
        return items.value[index]
    }
    
    //
    private func observeChanges() {
        guard let showId = episode.show?.id else {
            return
        }
        
        castRepository.getChanges(by: showId)
            .subscribe(onNext: { [unowned self] changes in
                self.emit(changes: changes)
            })
            .disposed(by: disposeBag)
    }
    
    private func emit(changes: CastChanges) {
        items.accept(changes.items.map {
            CastTableCellViewModel(cast: $0)
        })
        changeset.accept(changes.changeset ?? Changeset.empty())
    }
    
}
