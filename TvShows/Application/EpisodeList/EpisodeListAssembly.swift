//
//  EpisodesCollectionAssembly.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Swinject

struct EpisodeListAssembly: Assembly {
    
    func assemble(container: Container) {
        container.storyboardInitCompleted(EpisodeListViewController.self) { (r, c, data: HomeViewController.TabContext) in
            let viewModel = r.resolve(EpisodeListViewModel.self, argument: data)!
            c.viewModel = ViewModel(viewModel, viewModel)
        }
        
        container.register(EpisodeListViewModel.self) { (r, tabContext: HomeViewController.TabContext) in
            return EpisodeListViewModel(
                episodeRepository: r.resolve(EpisodeRepository.self)!,
                tabContext: tabContext
            )
        }
    }
    
}
