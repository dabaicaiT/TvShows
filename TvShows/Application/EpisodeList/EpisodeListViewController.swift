//
//  ViewController.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class EpisodeListViewController: UIViewController {
    
    @IBOutlet weak var showsCollectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var viewModel: ViewModel<EpisodeListViewModelInput, EpisodesListViewModelOutput>!
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupInput()
        setupOutput()
        
        viewModel.input.didLoad()
    }
    
    //
    private func setupViews() {
        let cellNib = UINib(nibName: EpisodeCollectionViewCell.nibName,
                            bundle: Bundle.main)
        showsCollectionView.register(cellNib,
                                     forCellWithReuseIdentifier: EpisodeCollectionViewCell.identifier)
    }
    
    private func setupInput() {
        showsCollectionView.rx.itemSelected
            .map { $0.row }
            .bind(to: viewModel.input.itemSelected)
            .disposed(by: disposeBag)
    }
    
    private func setupOutput() {
        viewModel.output.isLoading
            .drive(onNext: { [unowned self] loading in
                self.spinner.isHidden = !loading
            })
            .disposed(by: disposeBag)
        
        viewModel.output.changeset
            .asDriver()
            .drive(onNext: { [weak self] changes in
                self?.refreshCollectionView(with: changes, in: 0)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.enterDetail
            .subscribe(onNext: { [unowned self] episode in
                self.toDetailScreen(episode)
            })
            .disposed(by: disposeBag)
    }
    
    private func toDetailScreen(_ episode: Episode) {
        performSegue(withIdentifier: StoryboardSegue.EpisodeList.showDetail.rawValue,
                     data: episode)
    }
    
    private func refreshCollectionView(with changeset: Changeset, in section: Int) {
        if changeset.isEmpty {
            showsCollectionView.reloadData()
            return
        }
        
        let indexPath: (Int) -> IndexPath = { IndexPath(row: $0, section: section) }
        
        showsCollectionView.performBatchUpdates({
            showsCollectionView.insertItems(at: changeset.insertions.map(indexPath))
            showsCollectionView.deleteItems(at: changeset.deletions.map(indexPath))
            showsCollectionView.reloadItems(at: changeset.modifications.map(indexPath))
        })
    }
    
}

// MARK: UICollectionViewDataSource
extension EpisodeListViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return viewModel.output.itemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: EpisodeCollectionViewCell.identifier,
                                 for: indexPath) as! EpisodeCollectionViewCell
        let episode = viewModel.output.item(at: indexPath.row)
        episode.map(cell.set(viewModel:))
        return cell
    }
    
}

extension EpisodeListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.output.item(at: indexPath.row)?.getCellSize() ?? CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInset = EpisodeCollectionCellViewModel.Constants.edgeInset
        return UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
    }
    
}
