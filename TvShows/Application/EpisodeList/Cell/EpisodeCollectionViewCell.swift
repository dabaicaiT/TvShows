//
//  ShowCollectionViewCell.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class EpisodeCollectionViewCell: UICollectionViewCell {
    
    static let identifier = String(describing: EpisodeCollectionViewCell.self)
    static let nibName = String(describing: EpisodeCollectionViewCell.self)
    
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var networkLabel: UILabel!
    @IBOutlet weak var episodeInfoLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        resetViews()
    }
    
    override func prepareForReuse() {
        resetViews()
        
        super.prepareForReuse()
    }
    
    func set(viewModel: EpisodeCollectionCellViewModel) {
        previewImage.load(from: viewModel.previewUrl,
                          with: "placeholder_medium")
        
        viewModel.showName
            .drive(nameLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.networkName
            .drive(networkLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.episodeInfo
            .drive(episodeInfoLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.dateTime
            .drive(dateTimeLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    //
    private func resetViews() {
        previewImage.image = nil
        nameLabel.text = nil
        networkLabel.text = nil
        episodeInfoLabel.text = nil
    }
    
}
