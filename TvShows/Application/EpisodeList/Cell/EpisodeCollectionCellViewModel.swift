//
//  ShowCollectionViewCellViewModel.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class EpisodeCollectionCellViewModel {
    
    struct Constants {
        struct iPad {
            static let inlineCellCount: Int = 2
            static let cellHeight: CGFloat = 180
        }
        
        static let edgeInset: CGFloat = 20
        static let padding: CGFloat = 10
    }
    
    var showName: Driver<String> {
        return getShowName()
    }
    
    var networkName: Driver<String> {
        return getNetworkName()
    }
    
    var episodeInfo: Driver<String> {
        return getEpisodeInfo()
    }
    
    var dateTime: Driver<String> {
        return getDateTime()
    }
    
    var previewUrl: URL? {
        return getPreviewUrl()
    }
    
    let episode: Episode
    private let show: Show?
    
    init(episode: Episode) {
        self.episode = episode
        self.show = episode.show
    }
    
    func getCellSize() -> CGSize {
        let parentViewWidth = UIScreen.main.bounds.width - 2 * Constants.edgeInset
        
        var width: CGFloat, height: CGFloat = 0
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            width = getCellWidthOnPad(basedOn: parentViewWidth)
            height = Constants.iPad.cellHeight
        default:
            width = parentViewWidth
            height = getCellHeightOnPhone(constrained: width - 140)
        }
        
        return CGSize(width: width, height: height)
    }
    
    //
    private func getShowName() -> Driver<String> {
        return Observable
            .just(show?.name ?? "")
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getNetworkName() -> Driver<String> {
        return Observable
            .just(show?.network?.name ?? "")
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getEpisodeInfo() -> Driver<String> {
        let episodeInfo = "S\(episode.season?.description ?? "0")E\(episode.number?.description ?? "0")"
        return Observable
            .just(episodeInfo)
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getDateTime() -> Driver<String> {
        let date = episode.airdate?.split(separator: "-").dropFirst().joined(separator: "-") ?? ""
        return Observable
            .just("\(episode.airtime ?? "") \(date)")
            .asDriver(onErrorJustReturn: "")
    }
    
    private func getPreviewUrl() -> URL? {
        let urlString = show?.image?.medium
        return urlString == nil ? nil : URL(string: urlString!)
    }
    
    private func getCellHeightOnPhone(constrained width: CGFloat) -> CGFloat {
        let headFont = UIFont.preferredFont(forTextStyle: .headline)
        let subHeadFont = UIFont.preferredFont(forTextStyle: .subheadline)
        
        var height = show?.name.height(constrained: width,
                                       font: headFont) ?? 0
        height += Constants.padding
        height += show?.network?.name.height(constrained: width,
                                             font: subHeadFont) ?? 0
        height += Constants.padding
        height += episode.airtime?.height(constrained: width,
                                          font: subHeadFont) ?? 0
        height += Constants.padding
        height += episode.season?.description.height(constrained: width,
                                                     font: subHeadFont) ?? 0
        return height > 146 ? height + 40 : 182
    }
    
    private func getCellWidthOnPad(basedOn parent: CGFloat) -> CGFloat {
        var width = parent
        width -= CGFloat(Constants.iPad.inlineCellCount - 1) * Constants.edgeInset
        width = width / CGFloat(Constants.iPad.inlineCellCount)
        return width
    }
}
