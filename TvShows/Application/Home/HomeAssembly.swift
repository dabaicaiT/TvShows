//
//  HomeAssembly.swift
//  TvShows
//
//  Created by T on 2019-10-20.
//  Copyright © 2019 T. All rights reserved.
//

import Swinject

struct HomeAssembly: Assembly {
    
    func assemble(container: Container) {
        container.storyboardInitCompleted(HomeViewController.self) { r, c in
            let viewModel = r.resolve(HomeViewModel.self)!
            c.viewModel = ViewModel(viewModel, viewModel)
        }
        
        container.register(HomeViewModel.self) { r in
            let startDate = Date()
            return HomeViewModel(date: startDate,
                                 dateTimeFormatter: r.resolve(DateTimeFormatter.self)!)
        }
    }
    
}
