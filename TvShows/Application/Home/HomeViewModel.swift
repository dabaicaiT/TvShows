//
//  HomeViewModel.swift
//  TvShows
//
//  Created by T on 2019-10-20.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol HomeViewModelInput {
    var viewDidLoad: PublishSubject<Void> { get }
    var selectButton: PublishSubject<Int> { get }
    var filterContext: BehaviorRelay<String?> { get }
}

protocol HomeViewModelOutput {
    var buttonTitles: PublishSubject<[String]> { get }
    var loadContent: PublishSubject<HomeViewController.TabContext> { get }
}

final class HomeViewModel: HomeViewModelInput, HomeViewModelOutput {
    
    // input
    let viewDidLoad = PublishSubject<Void>()
    let selectButton = PublishSubject<Int>()
    let filterContext = BehaviorRelay<String?>(value: nil)
    
    // output
    let buttonTitles = PublishSubject<[String]>()
    let loadContent = PublishSubject<HomeViewController.TabContext>()
    
    private let date: Date!
    private let dateTimeFormatter: DateTimeFormatter
    private let disposeBag = DisposeBag()
    
    init(date: Date,
         dateTimeFormatter: DateTimeFormatter) {
        self.date = date
        self.dateTimeFormatter = dateTimeFormatter
        
        viewDidLoad
            .delay(.milliseconds(100), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [unowned self] in
                self.buttonTitles.onNext(self.getButtonTitles())
                self.selectButton.onNext(0)
            })
            .disposed(by: disposeBag)
        
        Observable.combineLatest(
            selectButton.distinctUntilChanged(),
            filterContext.distinctUntilChanged()
            )
            .map { [unowned self] (index, filter) in
                self.getTabContext(by: index, from: self.date, filter: filter)
            }
            .bind(to: loadContent)
            .disposed(by: disposeBag)
    }
    
    private func getTabContext(by index: Int,
                               from date: Date,
                               filter: String?) -> HomeViewController.TabContext {
        let date = dateTimeFormatter.getFormattedDate(from: date, by: index)
        return HomeViewController.TabContext(index: index,
                                             date: date,
                                             filter: filter)
    }
    
    private func getButtonTitles() -> [String] {
        var titles = ["Today"]
        for i in 1..<7 {
            titles.append(dateTimeFormatter.getShortWeekdaySymbol(from: self.date, by: i))
        }
        return titles
    }
    
}
