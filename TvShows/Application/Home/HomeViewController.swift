//
//  HomeViewController.swift
//  TvShows
//
//  Created by T on 2019-10-20.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class HomeViewController: UIViewController {
    
    @IBOutlet var dateButtons: [UIButton]!
    @IBOutlet weak var containerView: UIView!
    
    struct TabContext {
        let index: Int
        let date: String
        let filter: String?
    }
    
    var viewModel: ViewModel<HomeViewModelInput, HomeViewModelOutput>!
    
    private var viewControllerInContainer: UIViewController?
    private var listControllers: [EpisodeListViewController?] = [nil, nil, nil, nil, nil, nil, nil]
    private let searchController = UISearchController(searchResultsController: nil)
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupInput()
        setupOutput()
        
        viewModel.input.viewDidLoad.onNext(())
    }
    
    //
    private func setupViews() {
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        navigationItem.searchController = searchController
        
        let searchBar = searchController.searchBar
        searchBar.searchBarStyle = .minimal
        searchBar.barTintColor = .white
        
        searchBar.rx.text
            .bind(to: viewModel.input.filterContext)
            .disposed(by: disposeBag)
    }
    
    private func setupInput() {
        for (index, button) in dateButtons.enumerated() {
            button.rx.tap
                .map { index }
                .bind(to: viewModel.input.selectButton)
                .disposed(by: disposeBag)
        }
    }
    
    private func setupOutput() {
        viewModel.output.buttonTitles
            .subscribe(onNext: { [unowned self] titles in
                for (i, button) in self.dateButtons.enumerated() {
                    button.setTitle(titles[i], for: .normal)
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.output.loadContent
            .subscribe(onNext: { [unowned self] tabContext in
                self.handleLoadContent(tabContext)
            })
            .disposed(by: disposeBag)
    }
    
    private func handleLoadContent(_ tabContext: TabContext) {
        let index = tabContext.index
        self.selectedButton(index: index)
        self.listControllers[index] =
            StoryboardScene.EpisodeList.initialScene.instantiate(data: tabContext)
        self.displayInContainer(index: index)
    }
    
    private func selectedButton(index: Int) {
        for (i, button) in dateButtons.enumerated() {
            button.backgroundColor = i == index ?
                ColorName.buttonBackgroundHighlight.color :
                ColorName.buttonBackgroundNormal.color
        }
    }
    
    private func displayInContainer(index: Int) {
        guard let viewController = listControllers[index] else {
            return
        }
        
        viewControllerInContainer?.view.removeFromSuperview()
        viewControllerInContainer?.removeFromParent()
        viewControllerInContainer = viewController
        
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.didMove(toParent: self)
    }
    
}
