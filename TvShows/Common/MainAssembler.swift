//
//  MainAssembler.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard
import UIKit

final class MainAssembler {
    
    let container: Container
    let assembler: Assembler
    
    init() {
        Container.loggingFunction = nil
        
        container = SwinjectStoryboard.defaultContainer
        assembler = Assembler(container: container)
        
        assembler.apply(assembly: NetworkingAssembly())
        assembler.apply(assembly: AppUtilsAssembly())
        assembler.apply(assembly: AdapterAssembly())
        assembler.apply(assembly: RepositoryAssembly())
        assembler.apply(assembly: InteractorAssembly())
        assembler.apply(assembly: HomeAssembly())
        assembler.apply(assembly: EpisodeListAssembly())
        assembler.apply(assembly: ShowDetailAssembly())
    }
    
    func setupRealm() {
        resolveApi(RealmStorageSetup.self)!.setup(inMemoryIdentifier: nil)
    }
    
    func resolveApi<Api>(_ apiType: Api.Type) -> Api? {
        return SwinjectStoryboard.defaultContainer.resolve(apiType)
    }
    
}
