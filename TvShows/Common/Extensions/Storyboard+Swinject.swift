//
//  Storyboard+Swinject.swift
//  TvShows
//
//  Created by T on 2019-10-20.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import Swinject

extension InitialSceneType where T: UIViewController {
    func instantiate<A>(data: A) -> T {
        let controller = instantiate()
        controller.injectedDataReceived?(InjectedData(data: data))
        return controller
    }
}

extension SceneType where T: UIViewController {
    func instantiate<A>(data: A) -> T {
        let controller = instantiate()
        controller.injectedDataReceived?(InjectedData(data: data))
        return controller
    }
}
