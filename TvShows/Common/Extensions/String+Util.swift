//
//  String+Util.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit

extension String {
    
    func height(constrained width: CGFloat,
                font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [NSAttributedString.Key.font: font],
                                            context: nil)
        return ceil(boundingBox.height)
    }
    
    func plainTextFromHTML() -> String? {
        do {
            let regex = try NSRegularExpression(pattern: "<.*?>",
                                                options: NSRegularExpression.Options.caseInsensitive)
            let plainText = regex.stringByReplacingMatches(in: self,
                                                           options: NSRegularExpression.MatchingOptions.reportProgress,
                                                           range: NSMakeRange(0, self.count),
                                                           withTemplate: "")
            
            return plainText
        }
        catch {
            print("[Error] \(error)")
            return nil
        }
    }
    
    func toInternetDateTime() -> Date? {
        let formatter = ISO8601DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.formatOptions = [.withInternetDateTime]
        return formatter.date(from: self)
    }
    
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex,
                          options: .regularExpression,
                          range: nil,
                          locale: nil) != nil
    }
    
}
