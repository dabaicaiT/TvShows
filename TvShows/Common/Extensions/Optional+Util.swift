//
//  Optional+Util.swift
//  TvShows
//
//  Created by T on 2019-10-20.
//  Copyright © 2019 T. All rights reserved.
//

extension Swift.Optional where Wrapped == String {
    
    var isEmpty: Bool {
        guard let `self` = self else {
            return true
        }
        return self.trimmingCharacters(in: .whitespaces).isEmpty
    }
    
}
