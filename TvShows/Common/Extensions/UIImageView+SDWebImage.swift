//
//  UIImageView+.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func load(from url: URL?,
              with avatar: String) {
        self.sd_setImage(with: url,
                         placeholderImage: UIImage(named: avatar),
                         options: [])
    }
    
}
