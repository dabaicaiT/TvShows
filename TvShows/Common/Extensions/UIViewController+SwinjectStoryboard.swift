/*
 * open source from
 * https://gist.github.com/IanKeen/93e9bbf203f84407e7873044d765b164
 */

import UIKit
import Swinject
import SwinjectStoryboard

#if os(iOS) || os(OSX) || os(tvOS)
extension UIViewController {
    /// Configures the swizzling required to hook into the segue mechanism
    static let swizzleSegues: Void = {
        swizzle(UIViewController.self, #selector(prepare(for:sender:)), #selector(swizzled_prepare(for:sender:)))
    }()
    
    @objc private func swizzled_prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let data = sender as? InjectedData {
            segue.destination.injectedDataReceived?(data)
        }
        swizzled_prepare(for: segue, sender: sender)
    }
}

extension UIViewController {
    /// Initiates the segue with the specified identifier from the current view controller's storyboard file.
    ///
    /// - Parameters:
    ///   - identifier: The string that identifies the triggered segue. In Interface Builder,
    ///                 you specify the segue’s identifier string in the attributes inspector.
    ///                 This method throws an Exception handling if there is no segue with the specified identifier.
    ///   - data:       The object that you want to pass to `storyboardInitCompleted` during the segue.
    func performSegue(withIdentifier identifier: String, data: Any) {
        let sender = InjectedData(data: data)
        performSegue(withIdentifier: identifier, sender: sender)
    }
}

extension Container {
    /// Adds a registration of the specified view or window controller that is configured in a storyboard.
    ///
    /// - Note: This registration is ONLY called for segues triggered by `performSegue(withIdentifier:data:)`.
    ///
    /// - Note: Do NOT explicitly resolve the controller registered by this method.
    ///         The controller is intended to be resolved by `SwinjectStoryboard` implicitly.
    ///
    /// - Parameters:
    ///   - controllerType: The controller type to register as a service type.
    ///                     The type is `UIViewController` in iOS, `NSViewController` or `NSWindowController` in OS X.
    ///   - name:           A registration name, which is used to differentiate from other registrations
    ///                     that have the same view or window controller type.
    ///   - initCompleted:  A closure to specify how the dependencies of the view or window controller are injected.
    ///                     It is invoked by the `Container` when the view or window controller is instantiated by `SwinjectStoryboard`.
    func storyboardInitCompleted<C: Controller, A>(_ controllerType: C.Type, name: String? = nil, initCompleted: @escaping (Resolver, C, A) -> Void) {
        storyboardInitCompleted(C.self) { r, c in
            c.injectedDataReceived = { data in
                initCompleted(r, c, data.value())
            }
        }
    }
}
#endif

private func swizzle(_ `class`: AnyClass, _ originalSelector: Selector, _ swizzledSelector: Selector) {
    let originalMethod = class_getInstanceMethod(`class`, originalSelector)!
    let swizzledMethod = class_getInstanceMethod(`class`, swizzledSelector)!
    
    let didAdd = class_addMethod(
        `class`, originalSelector,
        method_getImplementation(swizzledMethod),
        method_getTypeEncoding(swizzledMethod)
    )
    
    if didAdd {
        class_replaceMethod(
            `class`, swizzledSelector,
            method_getImplementation(originalMethod),
            method_getTypeEncoding(originalMethod)
        )
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
}

class InjectedData {
    private let data: Any
    init(data: Any) { self.data = data }
    @nonobjc func value<T>() -> T { return data as! T }
}

extension UIViewController {
    var injectedDataReceived: ((InjectedData) -> Void)? {
        get { return self[dynamic: #function] }
        set { self[dynamic: #function] = newValue }
    }
}

private extension String {
    var unsafePointer: UnsafeRawPointer {
        return UnsafeRawPointer(bitPattern: hashValue)!
    }
}

protocol DynamicProperty: class {
    subscript<T>(dynamic key: String) -> T? { get set }
}

extension DynamicProperty {
    subscript<T>(dynamic key: String) -> T? {
        get {
            return objc_getAssociatedObject(self, key.unsafePointer) as? T
        }
        set {
            objc_setAssociatedObject(self, key.unsafePointer, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

extension NSObject: DynamicProperty {}
