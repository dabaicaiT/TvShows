//
//  ViewModel.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

final class ViewModel<Input, Output> {
    
    let input: Input
    let output: Output
    
    init(_ input: Input, _ output: Output) {
        self.input = input
        self.output = output
    }
    
}
