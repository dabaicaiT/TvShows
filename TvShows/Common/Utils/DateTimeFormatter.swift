//
//  DateFormatter.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

protocol DateTimeFormatter: class {
    func getFormattedDate(from date: Date?, by offset: Int) -> String
    func getShortWeekdaySymbol(from date: Date?, by offset: Int) -> String
}

final class DateTimeFormatterImpl: DateTimeFormatter {
    
    private let calendar = Calendar.current
    private let iso8601Formatter = ISO8601DateFormatter()
    private let formatter = DateFormatter()
    
    init() {}
    
    func getFormattedDate(from date: Date? = Date(),
                          by offset: Int) -> String {
        let newDate = calendar.date(byAdding: .day,
                                    value: offset,
                                    to: date!)
        return toString(from: newDate)
    }
    
    func getShortWeekdaySymbol(from date: Date? = Date(),
                               by offset: Int) -> String {
        let newDate = calendar.date(byAdding: .day,
                                    value: offset,
                                    to: date!)!
        let dayComponent = calendar.component(.weekday,
                                              from: newDate)
        return formatter.shortWeekdaySymbols[dayComponent - 1]
    }
    
    //
    private func toString(from date: Date? = Date()) -> String {
        iso8601Formatter.formatOptions = [.withFullDate, .withDashSeparatorInDate]
        iso8601Formatter.timeZone = TimeZone.current
        return iso8601Formatter.string(from: date!)
    }
    
}
