//
//  AppUtilsAssembly.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Swinject

struct AppUtilsAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(DateTimeFormatter.self) { r in
            return DateTimeFormatterImpl()
        }
        .inObjectScope(.container)
    }
    
}
