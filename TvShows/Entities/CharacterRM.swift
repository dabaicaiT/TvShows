//
//  CharacterRM.swift
//  TvShows
//
//  Created by T on 2019-10-20.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

final class CharacterRM: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func create(from character: Character) -> CharacterRM {
        let characterRM = CharacterRM()
        characterRM.id = character.id
        character.name.map { characterRM.name = $0 }
        return characterRM
    }
    
    func toCharacter() -> Character {
        return Character(id: id,
                         name: name)
    }
}

typealias CharacterRMDAO = RealmDAO<CharacterRM>
