//
//  Show.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Episode: Decodable {
    
    let id: Int
    let url: String?
    let name: String
    let season: Int?
    let number: Int?
    let airdate: String?
    let airtime: String?
    let airstamp: Date?
    let runtime: Int?
    let image: Image?
    let summary: String?
    let show: Show?
    
    private enum CodingKeys: String, CodingKey {
        case id, url, name, season, number, airdate, airtime, airstamp, runtime, image, summary, show
    }
    
    init(from decoder: Decoder) throws {
        let value = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try value.decode(Int.self, forKey: .id)
        url = try value.decodeIfPresent(String.self, forKey: .url)
        name = try value.decode(String.self, forKey: .name)
        season = try value.decodeIfPresent(Int.self, forKey: .season)
        number = try value.decodeIfPresent(Int.self, forKey: .number)
        airdate = try value.decodeIfPresent(String.self, forKey: .airdate)
        airtime = try value.decodeIfPresent(String.self, forKey: .airtime)
        airstamp = (try value.decodeIfPresent(String.self, forKey: .airstamp))?.toInternetDateTime()
        runtime = try value.decodeIfPresent(Int.self, forKey: .runtime)
        image = try value.decodeIfPresent(Image.self, forKey: .image)
        summary = try value.decodeIfPresent(String.self, forKey: .summary)
        show = try value.decodeIfPresent(Show.self, forKey: .show)
    }
    
    init(id: Int,
         url: String? = nil,
         name: String,
         season: Int? = nil,
         number: Int? = nil,
         airdate: String? = nil,
         airtime: String? = nil,
         airstamp: Date? = nil,
         runtime: Int? = nil,
         image: Image? = nil,
         summary: String? = nil,
         show: Show? = nil) {
        self.id = id
        self.url = url
        self.name = name
        self.season = season
        self.number = number
        self.airdate = airdate
        self.airtime = airtime
        self.airstamp = airstamp
        self.runtime = runtime
        self.image = image
        self.summary = summary
        self.show = show
    }
    
}
