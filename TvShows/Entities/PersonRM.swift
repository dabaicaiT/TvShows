//
//  PersonRM.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

final class PersonRM: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var mediumImage: String?
    @objc dynamic var originalImage: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func create(from person: Person) -> PersonRM {
        let personRM = PersonRM()
        personRM.id = person.id
        person.name.map { personRM.name = $0 }
        person.image.map {
            personRM.mediumImage = $0.medium
            personRM.originalImage = $0.original
        }
        return personRM
    }
    
    func toPerson() -> Person {
        let image = Image.create(mediumUrl: mediumImage,
                                 originalUrl: originalImage)
        return Person(id: id,
                      name: name,
                      image: image)
    }
    
}

typealias PersonRMDAO = RealmDAO<PersonRM>
