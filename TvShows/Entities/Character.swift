//
//  Character.swift
//  TvShows
//
//  Created by T on 2019-10-20.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Character: Decodable {
    let id: Int
    let name: String?
}
