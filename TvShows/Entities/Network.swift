//
//  Network.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Network: Decodable {
    let id: Int
    let name: String
    let country: Country?
}
