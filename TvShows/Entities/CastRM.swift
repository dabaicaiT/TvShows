//
//  CastRM.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

final class CastRM: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var showId: Int = 0
    @objc dynamic var person: PersonRM?
    @objc dynamic var character: CharacterRM?
    @objc dynamic var isSelf: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func create(from cast: Cast) -> CastRM {
        let castRM = CastRM()
        cast.id.map { castRM.id = $0 }
        cast.showId.map { castRM.showId = $0 }
        cast.person.map { castRM.person = PersonRM.create(from: $0) }
        cast.character.map { castRM.character = CharacterRM.create(from: $0) }
        castRM.isSelf = cast.isSelf
        return castRM
    }
    
    func toCast() -> Cast {
        return Cast(id: id,
                    showId: showId,
                    person: person?.toPerson(),
                    character: character?.toCharacter(),
                    isSelf: isSelf)
    }
    
}

typealias CastRMDAO = RealmDAO<CastRM>
typealias CastChanges = EntityChanges<Cast>
