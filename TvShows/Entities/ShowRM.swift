//
//  ShowRM.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

final class ShowRM: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var type: String?
    @objc dynamic var genres: String?
    @objc dynamic var runtime: Int = 0
    @objc dynamic var premiered: String?
    @objc dynamic var officialSite: String?
    @objc dynamic var network: NetworkRM?
    @objc dynamic var mediumImage: String?
    @objc dynamic var originalImage: String?
    @objc dynamic var summary: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func create(from show: Show) -> ShowRM {
        let showRM = ShowRM()
        showRM.id = show.id
        showRM.name = show.name
        show.type.map { showRM.type = $0 }
        show.genres.map { showRM.genres = $0.joined(separator: ",") }
        show.runtime.map { showRM.runtime = $0 }
        show.premiered.map { showRM.premiered = $0 }
        show.officialSite.map { showRM.officialSite = $0 }
        show.network.map { showRM.network = NetworkRM.create(from: $0) }
        show.image.map {
            showRM.mediumImage = $0.medium
            showRM.originalImage = $0.original
        }
        show.summary.map { showRM.summary = $0 }
        return showRM
    }
    
    func toShow() -> Show {
        let image = Image.create(mediumUrl: mediumImage,
                                 originalUrl: originalImage)
        let genresArray = genres?.split(separator: ",").map { String($0) }
        return Show(id: id,
                    name: name ?? "",
                    type: type ?? "",
                    genres: genresArray ?? [],
                    runtime: runtime,
                    premiered: premiered,
                    officialSite: officialSite ?? "",
                    network: network?.toNetwork(),
                    image: image,
                    summary: summary)
    }
    
}

typealias ShowRMDAO = RealmDAO<ShowRM>
