//
//  Image.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Image: Decodable {
    let medium: String
    let original: String
}


extension Image {
    
    static func create(mediumUrl: String?,
                       originalUrl: String?) -> Image? {
        guard let mediumUrl = mediumUrl,
            let originalUrl = originalUrl else {
                return nil
        }
        
        return Image(medium: mediumUrl,
                     original: originalUrl)
    }
    
}
