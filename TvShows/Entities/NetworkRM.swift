//
//  NetworkRM.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

final class NetworkRM: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var countryName: String?
    @objc dynamic var countryCode: String?
    @objc dynamic var countryTimezone: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func create(from network: Network) -> NetworkRM {
        let networkRM = NetworkRM()
        networkRM.id = network.id
        networkRM.name = network.name
        network.country.map {
            networkRM.countryName = $0.name
            networkRM.countryCode = $0.code
            networkRM.countryTimezone = $0.timezone
        }
        return networkRM
    }
    
    func toNetwork() -> Network {
        var country: Country?
        if countryName != nil &&
            countryCode != nil &&
            countryTimezone != nil {
            country = Country(name: countryName!,
                              code: countryCode!,
                              timezone: countryTimezone!)
        }
        return Network(id: id,
                       name: name ?? "",
                       country: country)
    }
    
}

typealias NetworkRMDAO = RealmDAO<NetworkRM>
