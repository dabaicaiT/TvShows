//
//  Person.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Cast: Decodable {
    
    var id: Int?
    var showId: Int?
    let person: Person?
    let character: Character?
    let isSelf: Bool
    
    private enum CodingKeys: String, CodingKey {
        case showId, person, character, isSelf = "self"
    }
    
}
