//
//  Country.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Country: Decodable {
    let name: String
    let code: String
    let timezone: String
}
