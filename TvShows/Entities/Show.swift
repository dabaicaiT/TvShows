//
//  Show.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Show: Decodable {
    let id: Int
    let name: String
    let type: String?
    let genres: [String]?
    let runtime: Int?
    let premiered: String?
    let officialSite: String?
    let network: Network?
    let image: Image?
    let summary: String?
}
