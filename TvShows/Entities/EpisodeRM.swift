//
//  EpisodeRM.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

final class EpisodeRM: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var url: String?
    @objc dynamic var name: String?
    @objc dynamic var season: Int = 0
    @objc dynamic var number: Int = 0
    @objc dynamic var airdate: String?
    @objc dynamic var airtime: String?
    @objc dynamic var airstamp: Date?
    @objc dynamic var runtime: Int = 0
    @objc dynamic var mediumImage: String?
    @objc dynamic var originalImage: String?
    @objc dynamic var summary: String?
    @objc dynamic var show: ShowRM?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func create(from episode: Episode) -> EpisodeRM {
        let episodeRM = EpisodeRM()
        episodeRM.id = episode.id
        episode.url.map { episodeRM.url = $0 }
        episodeRM.name = episode.name
        episode.season.map { episodeRM.season = $0 }
        episode.number.map { episodeRM.number = $0 }
        episode.airdate.map { episodeRM.airdate = $0 }
        episode.airtime.map { episodeRM.airtime = $0 }
        episode.airstamp.map { episodeRM.airstamp = $0 }
        episode.runtime.map { episodeRM.runtime = $0 }
        episode.image.map {
            episodeRM.mediumImage = $0.medium
            episodeRM.originalImage = $0.original
        }
        episode.summary.map { episodeRM.summary = $0 }
        episode.show.map { episodeRM.show = ShowRM.create(from: $0) }
        return episodeRM
    }
    
    func toEpisode() -> Episode {
        let image = Image.create(mediumUrl: mediumImage,
                                 originalUrl: originalImage)
        return Episode(id: id,
                       url: url,
                       name: name ?? "",
                       season: season,
                       number: number,
                       airdate: airdate,
                       airtime: airtime,
                       airstamp: airstamp,
                       runtime: runtime,
                       image: image,
                       summary: summary ?? "",
                       show: show?.toShow())
    }
    
}

typealias EpisodeRMDAO = RealmDAO<EpisodeRM>
typealias EpisodeChanges = EntityChanges<Episode>
