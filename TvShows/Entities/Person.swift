//
//  Person.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Person: Decodable {
    let id: Int
    let name: String?
    let image: Image?
}
