//
//  AdapterAssembly.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Swinject

struct AdapterAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(RealmStorageSetup.self) { _ in
            return RealmStorageSetupImpl()
        }
    }
    
}
