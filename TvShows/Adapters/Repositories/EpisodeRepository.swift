//
//  EpisodeRepository.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm
import RxSwift

protocol EpisodeRepository: class {
    var isLoading: PublishSubject<Bool> { get }
    func getChanges(by date: String, filter: String?) -> Observable<EpisodeChanges>
}

final class RealmEpisodeRepositoryImpl: EpisodeRepository {
    
    let isLoading = PublishSubject<Bool>()
    
    private var episodeRMDAO: EpisodeRMDAO {
        return EpisodeRMDAO(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private var episodeRMAssociation: EpisodeRMAssociation {
        return EpisodeRMAssociation(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private let getEpisodeInteractor: GetEpisodeInteractor!
    private let inMemoryIdentifier: String?
    private var interactorDisposable: Disposable?
    
    init(getEpisodeInteractor: GetEpisodeInteractor,
         inMemoryIdentifier: String? = nil) {
        self.getEpisodeInteractor = getEpisodeInteractor
        self.inMemoryIdentifier = inMemoryIdentifier
    }
    
    func getChanges(by date: String,
                    filter: String? = nil) -> Observable<EpisodeChanges> {
        return Observable
            .changeset(from: getSortedChanges(by: date, filter: filter))
            .map { result, changeset in
                return EpisodeChanges(
                    items: result.map { $0.toEpisode() },
                    changeset: changeset?.toEntityChangeset()
                )
            }
            .startWith(EpisodeChanges.empty())
    }
    
    //
    private func save(_ episodes: [Episode]) {
        episodes.forEach {
            let episodeRm = EpisodeRM.create(from: $0)
            episodeRMDAO.save(model: episodeRm)
            episodeRMAssociation.update(for: episodeRm)
        }
    }
    
    private func getSortedChanges(by date: String,
                                  filter: String?) -> Results<EpisodeRM> {
        let query = buildQuery(by: date, filter: filter)
        let result = episodeRMDAO.get(query: query)
            .sorted(byKeyPath: "airstamp", ascending: true)
        
        if filter.isEmpty && result.isEmpty {
            fetchEpisodes(by: date)
        }
        return result
    }
    
    private func buildQuery(by date: String, filter: String?) -> String {
        return filter.isEmpty ?
            "airdate = '\(date)'" :
        "airdate = '\(date)' && (name CONTAINS[c] '\(filter!)' || show.name CONTAINS[c] '\(filter!)' || show.network.name CONTAINS[c] '\(filter!)')"
    }
    
    private func fetchEpisodes(by date: String) {
        isLoading.onNext(true)
        
        interactorDisposable?.dispose()
        
        interactorDisposable = getEpisodeInteractor.execute(date: date)
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { [unowned self] episodes in
                guard let episodes = episodes else {
                    return
                }
                self.save(episodes)
                self.isLoading.onNext(false)
            })
    }
    
}
