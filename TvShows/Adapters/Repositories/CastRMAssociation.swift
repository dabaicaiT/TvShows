//
//  CastRMAssociation.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

final class CastRMAssociation {
    
    private var castRMDAO: CastRMDAO {
        return CastRMDAO(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private var personRMDAO: PersonRMDAO {
        return PersonRMDAO(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private var characterRMDAO: CharacterRMDAO {
        return CharacterRMDAO(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private let inMemoryIdentifier: String?
    
    init(inMemoryIdentifier: String? = nil) {
        self.inMemoryIdentifier = inMemoryIdentifier
    }
    
    func update(for cast: CastRM) {
        updatePerson(for: cast.makeCopy())
        castRMDAO.save(model: cast)
    }
    
    //
    private func updatePerson(for cast: CastRM) {
        if let personRM = cast.person {
            personRMDAO.save(model: personRM)
        }
        if let characterRM = cast.character {
            characterRMDAO.save(model: characterRM)
        }
    }
    
}
