//
//  EpisodeRMAssociation.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

final class EpisodeRMAssociation {
    
    private var episodeRMDAO: EpisodeRMDAO {
        return EpisodeRMDAO(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private var showRMDAO: ShowRMDAO {
        return ShowRMDAO(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private let inMemoryIdentifier: String?
    
    init(inMemoryIdentifier: String? = nil) {
        self.inMemoryIdentifier = inMemoryIdentifier
    }
    
    func update(for episode: EpisodeRM) {
        updateShow(for: episode.makeCopy())
        episodeRMDAO.save(model: episode)
    }
    
    //
    private func updateShow(for episode: EpisodeRM) {
        guard let showRm = episode.show else {
            return
        }
        showRMDAO.save(model: showRm)
    }
    
}
