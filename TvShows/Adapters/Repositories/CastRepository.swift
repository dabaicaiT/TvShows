//
//  CastRepository.swift
//  TvShows
//
//  Created by T on 2019-10-18.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm
import RxSwift

protocol CastRepository: class {
    func getChanges(by showId: Int) -> Observable<CastChanges>
}

final class RealmCastRepositoryImpl: CastRepository {
    
    private var castRMDAO: CastRMDAO {
        return CastRMDAO(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private var castRMAssociation: CastRMAssociation {
        return CastRMAssociation(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    private let getCastInteractor: GetCastInteractor!
    private let inMemoryIdentifier: String?
    private var requestDisposable: Disposable?
    
    init(getCastInteractor: GetCastInteractor,
         inMemoryIdentifier: String? = nil) {
        self.getCastInteractor = getCastInteractor
        self.inMemoryIdentifier = inMemoryIdentifier
    }
    
    func getChanges(by showId: Int) -> Observable<CastChanges> {
        return Observable
            .changeset(from: getSortedChanges(by: showId))
            .map { result, changeset in
                return CastChanges(
                    items: result.map { $0.toCast() },
                    changeset: changeset?.toEntityChangeset()
                )
            }
            .startWith(CastChanges.empty())
    }
    
    //
    private func save(showId: Int, _ casts: [Cast]) {
        for (index, cast) in casts.enumerated() {
            var cast = cast
            cast.id = Int(Date().timeIntervalSince1970) * 10 + index
            cast.showId = showId
            save(cast)
        }
    }
    
    private func save(_ cast: Cast) {
        let castRm = CastRM.create(from: cast)
        castRMDAO.save(model: castRm)
        castRMAssociation.update(for: castRm)
    }
    
    private func getSortedChanges(by showId: Int) -> Results<CastRM> {
        let result = castRMDAO.get(query: "showId = \(showId)")
            .sorted(byKeyPath: "id", ascending: true)
        
        if result.isEmpty {
            fetchCasts(by: showId)
        }
        return result
    }
    
    private func fetchCasts(by showId: Int) {
        requestDisposable?.dispose()
        
        requestDisposable = getCastInteractor.execute(showId: showId)
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { [unowned self] casts in
                guard let casts = casts else {
                    return
                }
                self.save(showId: showId, casts)
            })
    }
    
}
