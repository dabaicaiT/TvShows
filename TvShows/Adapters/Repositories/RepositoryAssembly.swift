//
//  RepositoryAssembly.swift
//  TvShows
//
//  Created by T on 2019-10-19.
//  Copyright © 2019 T. All rights reserved.
//

import Swinject

struct RepositoryAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(EpisodeRepository.self) { r in
            return RealmEpisodeRepositoryImpl(
                getEpisodeInteractor: r.resolve(GetEpisodeInteractor.self)!,
                inMemoryIdentifier: nil
            )
        }
        
        container.register(CastRepository.self) { r in
            return RealmCastRepositoryImpl(
                getCastInteractor: r.resolve(GetCastInteractor.self)!,
                inMemoryIdentifier: nil
            )
        }
    }
    
}
