//
//  Realm+Setup.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    
    static func setup() {
        Realm.Configuration.defaultConfiguration = createConfig()
    }
    
    //
    private static func createConfig() -> Realm.Configuration {
        return Realm.Configuration(
            fileURL: RealmConfigurationValues.fileURL,
            schemaVersion: RealmConfigurationValues.schemaVersion,
            deleteRealmIfMigrationNeeded: true,
            shouldCompactOnLaunch: { totalBytes, usedBytes in
                let oneHundredMB = 100 * 1024 * 1024
                return (totalBytes > oneHundredMB) && (Double(usedBytes) / Double(totalBytes)) < 0.5
        })
    }
    
}
