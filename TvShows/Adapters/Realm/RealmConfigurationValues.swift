//
//  RealmConfigurationValues.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct RealmConfigurationValues {
    
    static let schemaVersion: UInt64 = 1
    
    static var fileURL: URL {
        let libraryPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory,
                                                              .userDomainMask,
                                                              true)
            .first ?? ""
        print("Path: \(libraryPath)")
        return URL(fileURLWithPath: libraryPath).appendingPathComponent("db.realm")
    }
    
}
