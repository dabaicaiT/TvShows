//
//  RealmChangeset+Util.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RxRealm

extension RealmChangeset {
    
    func toEntityChangeset() -> Changeset {
        return Changeset(deletions: deleted,
                         insertions: inserted,
                         modifications: updated)
    }
    
}
