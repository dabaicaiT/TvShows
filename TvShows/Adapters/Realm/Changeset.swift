//
//  Changeset.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct Changeset {
    
    let deletions: [Int]
    let insertions: [Int]
    let modifications: [Int]
    
    var isEmpty: Bool {
        return deletions.isEmpty &&
            insertions.isEmpty &&
            modifications.isEmpty
    }
    
}

extension Changeset {
    
    static func empty() -> Changeset {
        return Changeset(deletions: [],
                         insertions: [],
                         modifications: [])
    }
    
}
