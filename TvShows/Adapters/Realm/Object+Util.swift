//
//  Object+Util.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

extension Object {
    
    func makeCopy() -> Self {
        return type(of: self).init(value: self,
                                   schema: .partialPrivateShared())
    }
    
}
