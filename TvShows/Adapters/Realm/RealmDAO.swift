//
//  RealmDAO.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

class RealmDAO<Model: Object> {
    
    var count: Int {
        return realm.objects(Model.self).count
    }
    
    var isEmpty: Bool {
        return all().isEmpty
    }
    
    private let realm: Realm
    
    init(inMemoryIdentifier: String? = nil) {
        realm = Realm.tryCreate(with: inMemoryIdentifier)
    }
    
    func get(query: String) -> Results<Model> {
        return all().filter(query)
    }
    
    func save(model: Model) {
        realm.tryWrite {
            realm.add(model, update: true)
        }
    }
    
    private func all() -> Results<Model> {
        return realm.objects(Model.self)
    }
    
}
