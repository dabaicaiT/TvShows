//
//  EntityChanges.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation

struct EntityChanges<T> {
    let items: [T]
    let changeset: Changeset?
}

extension EntityChanges {
    
    static func empty() -> EntityChanges<T> {
        return EntityChanges<T>(items: [],
                                changeset: Changeset.empty())
    }
    
}
