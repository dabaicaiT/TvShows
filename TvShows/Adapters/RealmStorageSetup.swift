//
//  RealmStorageSetup.swift
//  TvShows
//
//  Created by T on 2019-10-17.
//  Copyright © 2019 T. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmStorageSetup: class {
    func setup(inMemoryIdentifier: String?)
}

final class RealmStorageSetupImpl: RealmStorageSetup {
    
    init() {}
    
    func setup(inMemoryIdentifier: String? = nil) {
        initialOpenRealm(inMemoryIdentifier: inMemoryIdentifier)
    }
    
    //
    private func initialOpenRealm(inMemoryIdentifier: String? = nil) {
        do {
            Realm.setup()
            _ = try Realm.create(with: inMemoryIdentifier)
        }
        catch {
            print("[Realm] Unexpected error: \(error.localizedDescription)")
            removeExistingRealmFile()
            fatalError(error.localizedDescription)
        }
    }
    
    private func removeExistingRealmFile() {
        guard let realmFileUrl = Realm.Configuration.defaultConfiguration.fileURL else {
            return
        }
        
        do {
            try FileManager.default.removeItem(at: realmFileUrl)
        }
        catch {
            print("[Realm] Unexpected error: \(error.localizedDescription)")
        }
    }
    
}
