//
//  ShowDetailViewModelTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import RxSwift
import RxBlocking
import Cuckoo
import Nimble

class ShowDetailViewModelTests: XCTestCase {
    
    private let castRepository = MockCastRepository()
    private let casts = [Cast.sample()]
    private var disposeBag = DisposeBag()
    
    private var sut: ShowDetailViewModel!
    
    override func setUp() {
        super.setUp()
        
        sut = ShowDetailViewModel(castRepository: castRepository,
                                  episode: Episode.sample())
    }
    
    override func tearDown() {
        disposeBag = DisposeBag()
        
        super.tearDown()
    }
    
    func testDidLoad_whenChangeSetEmpty() {
        let changes = EntityChanges(items: casts, changeset: Changeset.empty())
        stub(castRepository) { stub in
            when(stub.getChanges(by: any())).thenReturn(.just(changes))
        }
        
        let result = try! sut.changeset.toBlocking().first()!
        expect(result.isEmpty).toEventually(beTrue(), timeout: 1)
    }
    
    func testDidLoad() {
        let changes = EntityChanges(items: casts, changeset: Changeset(deletions: [], insertions: [0], modifications: []))
        stub(castRepository) { stub in
            when(stub.getChanges(by: any())).thenReturn(.just(changes))
        }
        
        sut.didLoad()
        
        let result = try! sut.changeset.toBlocking().first()!
        expect(result.isEmpty).toEventuallyNot(beTrue(), timeout: 1)
    }
    
    func testItemsCount() {
        let changes = EntityChanges(items: casts, changeset: Changeset(deletions: [], insertions: [0], modifications: []))
        stub(castRepository) { stub in
            when(stub.getChanges(by: any())).thenReturn(.just(changes))
        }
        
        sut.didLoad()
        
        let result = sut.itemsCount
        expect(result).to(equal(1))
    }
    
}
