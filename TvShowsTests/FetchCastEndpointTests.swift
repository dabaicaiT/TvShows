//
//  FetchCastEndpointTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import Nimble
import Alamofire

class FetchCastEndpointTests: XCTestCase {
    
    func testInt() {
        let request = FetchCastEndpoint.Request(id: 111)
        
        expect(request.headers).to(beNil())
        expect(request.path).to(equal("/shows/111/cast"))
        expect(request.httpMethod).to(equal(HTTPMethod.get))
        expect(request.parameters).to(beNil())
    }
    
}

