//
//  HomeViewModelTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import RxSwift
import RxBlocking
import Nimble

class HomeViewModelTests: XCTestCase {
    
    private let episode = Episode.sample()
    private var disposeBag = DisposeBag()
    
    private var sut: HomeViewModel!
    
    override func setUp() {
        super.setUp()
        
        sut = HomeViewModel(date: Date(),
                            dateTimeFormatter: DateTimeFormatterImpl())
    }
    
    override func tearDown() {
        disposeBag = DisposeBag()
        
        super.tearDown()
    }
    
    func testLoadContent() {
        sut.viewDidLoad.onNext(())
        
        let result = try! sut.loadContent.toBlocking().first()!
        
        expect(result.index).toEventually(equal(0), timeout: 1)
    }
    
}
