//
//  CastTableCellViewModelTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import RxSwift
import RxBlocking
import Cuckoo
import Nimble

class CastTableCellViewModelTests: XCTestCase {
    
    private let cast = Cast.sample()
    private var disposeBag = DisposeBag()
    
    private var sut: CastTableCellViewModel!
    
    override func setUp() {
        super.setUp()
        
        sut = CastTableCellViewModel(cast: cast)
    }
    
    override func tearDown() {
        disposeBag = DisposeBag()
        
        super.tearDown()
    }
    
    func testPersonName() {
        let result = try! sut.personName.toBlocking().last()!
        
        expect(result).toEventually(equal(cast.person?.name), timeout: 1)
    }
    
    func testCharacterName() {
        let result = try! sut.characterName.toBlocking().last()!
        
        expect(result).toEventually(equal(cast.character?.name), timeout: 1)
    }
    
    func testNoCharacterWhenIsSelf() {
        let result = try! sut.isSelf.toBlocking().last()!
        
        expect(result).toEventually(equal(cast.isSelf), timeout: 1)
    }
    
    func testAvatarUrl() {
        let result = sut.avatarUrl
        
        expect(result?.absoluteString).to(contain("medium"))
    }
}
