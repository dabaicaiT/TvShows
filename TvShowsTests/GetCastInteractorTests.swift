//
//  GetCastInteractorTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import RxSwift
import RxBlocking
import Cuckoo
import Nimble

class GetCastInteractorTests: XCTestCase {
    
    private let tvMazeGateway = MockTvMazeGateway()
    private let mockCasts = [Cast.sample()]
    private var disposeBag = DisposeBag()
    
    private var sut: GetCastInteractor!
    
    override func setUp() {
        super.setUp()
        
        sut = GetCastInteractorImpl(tvMazeGateway: tvMazeGateway)
    }
    
    override func tearDown() {
        disposeBag = DisposeBag()
        
        super.tearDown()
    }
    
    func testExecuteReturnNil_whenShowIdNil() {
        let result = try! sut.execute(showId: nil).toBlocking().first()!
        expect(result).toEventually(beNil(), timeout: 1)
    }
    
    func testExecuteReturnNilFirst_whenShowIdNotNil() {
        stub(tvMazeGateway) { stub in
            when(stub.fetchCast(by: any())).thenReturn(.just(ApiServiceResult.success(mockCasts)))
        }
        
        let result = try! sut.execute(showId: 0).toBlocking().first()!
        expect(result).toEventually(beNil(), timeout: 1)
    }
    
    func testExecuteReturnNil_whenError() {
        let error = NSError(domain: "test", code: 999, userInfo: nil)
        stub(tvMazeGateway) { stub in
            when(stub.fetchCast(by: any())).thenReturn(.just(ApiServiceResult.error(error)))
        }
        
        let result = try! sut.execute(showId: 0).toBlocking().last()!
        expect(result).toEventually(beNil(), timeout: 1)
    }
    
    func testExecute() {
        stub(tvMazeGateway) { stub in
            when(stub.fetchCast(by: any())).thenReturn(.just(ApiServiceResult.success(mockCasts)))
        }
        
        let result = try! sut.execute(showId: 0).toBlocking().last()!
        expect(result).toEventuallyNot(beNil(), timeout: 1)
    }
    
}
