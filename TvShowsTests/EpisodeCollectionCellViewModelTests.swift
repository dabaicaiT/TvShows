//
//  EpisodeCollectionCellViewModelTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import RxSwift
import RxBlocking
import Cuckoo
import Nimble

class EpisodeCollectionCellViewModelTests: XCTestCase {
    
    private let episode = Episode.sample()
    private var disposeBag = DisposeBag()
    
    private var sut: EpisodeCollectionCellViewModel!
    
    override func setUp() {
        super.setUp()
        
        sut = EpisodeCollectionCellViewModel(episode: episode)
    }
    
    override func tearDown() {
        disposeBag = DisposeBag()
        
        super.tearDown()
    }
    
    func testShowName() {
        let result = try! sut.showName.toBlocking().last()!
        
        expect(result).toEventually(equal(episode.show!.name), timeout: 1)
    }
    
    func testNetworkName() {
        let result = try! sut.networkName.toBlocking().last()!
        
        expect(result).toEventually(equal(episode.show!.network!.name), timeout: 1)
    }
    
    func testEpisodeInfo() {
        let result = try! sut.episodeInfo.toBlocking().last()!
        
        expect(result).toEventually(contain(episode.season!.description,
                                            episode.number!.description),
                                    timeout: 1)
    }
    
    func testPreviewUrl() {
        let result = sut.previewUrl
        
        expect(result?.absoluteString).to(contain("medium"))
    }
}
