
//
//  Person+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows

extension Person {
    
    static func sample(id: Int? = 1,
                       name: String? = "person",
                       image: Image? = Image.sample()) -> Person {
        return Person(id: id!,
                      name: name,
                      image: image)
    }
    
}
