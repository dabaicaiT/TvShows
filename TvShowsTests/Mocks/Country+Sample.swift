//
//  Country+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows

extension Country {
    
    static func sample(name: String? = "country",
                       code: String? = "code",
                       timezone: String? = "timezone") -> Country {
        return Country(name: name!,
                       code: code!,
                       timezone: timezone!)
    }
    
}
