//
//  Show+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows

extension Show {
    
    static func sample(id: Int? = 1,
                       name: String? = "show",
                       type: String? = "type",
                       genres: [String]? = nil,
                       runtime: Int? = 60,
                       premiered: String? = "premiered",
                       officialSite: String? = nil,
                       network: Network? = Network.sample(),
                       image: Image? = Image.sample(),
                       summary: String? = nil) -> Show {
        return Show(id: id!,
                    name: name!,
                    type: type,
                    genres: genres,
                    runtime: runtime,
                    premiered: premiered,
                    officialSite: officialSite,
                    network: network,
                    image: image,
                    summary: summary)
    }
    
}
