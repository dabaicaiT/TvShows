//
//  Cast+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows

extension Cast {
    
    static func sample(id: Int? = 1,
                       showId: Int? = 1,
                       person: Person? = Person.sample(),
                       character: Character? = Character.sample(),
                       isSelf: Bool? = false) -> Cast {
        return Cast(id: id,
                    showId: showId,
                    person: person,
                    character: character,
                    isSelf: isSelf!)
    }
    
}
