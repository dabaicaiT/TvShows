//
//  TabContext+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows

extension HomeViewController.TabContext {
    
    static func sample(index: Int? = 1,
                       date: String? = "2019-10-11",
                       filter: String? = nil) -> HomeViewController.TabContext {
        return HomeViewController.TabContext(index: index!,
                                             date: date!,
                                             filter: filter)
    }
    
}
