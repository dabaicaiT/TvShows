//
//  Episode+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows
import Foundation

extension Episode {
    
    static func sample(id: Int? = 1,
                       url: String? = nil,
                       name: String? = "episode",
                       season: Int? = 2019,
                       number: Int? = 10,
                       airdate: String? = nil,
                       airtime: String? = nil,
                       airstamp: Date? = nil,
                       runtime: Int? = 30,
                       image: Image? = Image.sample(),
                       summary: String? = nil,
                       show: Show? = Show.sample()) -> Episode {
        return Episode(id: id!,
                       url: url,
                       name: name!,
                       season: season,
                       number: number,
                       airdate: airdate,
                       airtime: airtime,
                       airstamp: airstamp,
                       runtime: runtime,
                       image: image,
                       summary: summary,
                       show: show)
    }
    
}
