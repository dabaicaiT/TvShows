//
//  Character+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows

extension Character {
    
    static func sample(id: Int? = 1,
                       name: String? = "character") -> Character {
        return Character(id: id!,
                         name: name)
    }
    
}
