//
//  Image+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows

extension Image {
    
    static func sample(medium: String? = "http://www.test.com/medium",
                       original: String? = "http://www.test.com/original") -> Image {
        return Image(medium: medium!,
                     original: original!)
    }
    
}
