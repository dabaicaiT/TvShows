//
//  Network+Sample.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

@testable import TvShows

extension Network {
    
    static func sample(id: Int? = 1,
                       name: String? = "network",
                       country: Country? = Country.sample()) -> Network {
        return Network(id: id!,
                       name: name!,
                       country: country)
    }
    
}
