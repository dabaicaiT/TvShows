//
//  GetEpisodeInteractorTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import RxSwift
import RxBlocking
import Cuckoo
import Nimble

class GetEpisodeInteractorTests: XCTestCase {
    
    private let tvMazeGateway = MockTvMazeGateway()
    private let mockEpisodes = [Episode.sample()]
    private var disposeBag = DisposeBag()
    
    private var sut: GetEpisodeInteractor!
    
    override func setUp() {
        super.setUp()
        
        sut = GetEpisodeInteractorImpl(tvMazeGateway: tvMazeGateway)
    }
    
    override func tearDown() {
        disposeBag = DisposeBag()
        
        super.tearDown()
    }
    
    func testExecuteReturnNil_whenDateEmpty() {
        let result = try! sut.execute(date: "").toBlocking().first()!
        expect(result).toEventually(beNil(), timeout: 1)
    }
    
    func testExecuteReturnNil_whenDateIsWrongFormat() {
        let result = try! sut.execute(date: "0-0-0").toBlocking().first()!
        expect(result).toEventually(beNil(), timeout: 1)
    }
    
    func testExecuteReturnNilFirst_whenDateNotEmpty() {
        stub(tvMazeGateway) { stub in
            when(stub.fetchEpisode(by: any())).thenReturn(.just(ApiServiceResult.success(mockEpisodes)))
        }
        
        let result = try! sut.execute(date: "2017-10-21").toBlocking().first()!
        expect(result).toEventually(beNil(), timeout: 1)
    }
    
    func testExecuteReturnNil_whenError() {
        let error = NSError(domain: "test", code: 999, userInfo: nil)
        stub(tvMazeGateway) { stub in
            when(stub.fetchEpisode(by: any())).thenReturn(.just(ApiServiceResult.error(error)))
        }
        
        let result = try! sut.execute(date: "2017-10-21").toBlocking().last()!
        expect(result).toEventually(beNil(), timeout: 1)
    }
    
    func testExecute() {
        stub(tvMazeGateway) { stub in
            when(stub.fetchEpisode(by: any())).thenReturn(.just(ApiServiceResult.success(mockEpisodes)))
        }
        
        let result = try! sut.execute(date: "2017-10-21").toBlocking().last()!
        expect(result).toEventuallyNot(beNil(), timeout: 1)
    }
    
}
