//
//  EpisodeListViewModelTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import RxSwift
import RxBlocking
import Cuckoo
import Nimble

class EpisodeListViewModelTests: XCTestCase {
    
    private let episodeRepository = MockEpisodeRepository()
    private let tabContext = HomeViewController.TabContext.sample()
    private let episodes = [Episode.sample()]
    private var disposeBag = DisposeBag()
    
    private var sut: EpisodeListViewModel!
    
    override func setUp() {
        super.setUp()
        
        stub(episodeRepository) { stub in
            when(stub.isLoading.get).thenReturn(PublishSubject<Bool>())
        }
        
        sut = EpisodeListViewModel(episodeRepository: episodeRepository,
                                   tabContext: tabContext)
    }
    
    override func tearDown() {
        disposeBag = DisposeBag()
        
        super.tearDown()
    }
    
    func testDidLoad_whenChangeSetEmpty() {
        let changes = EntityChanges(items: episodes, changeset: Changeset.empty())
        stub(episodeRepository) { stub in
            when(stub.getChanges(by: any(), filter: any())).thenReturn(.just(changes))
        }
        
        let result = try! sut.changeset.toBlocking().first()!
        expect(result.isEmpty).toEventually(beTrue(), timeout: 1)
    }
    
    func testDidLoad() {
        let changes = EntityChanges(items: episodes, changeset: Changeset(deletions: [], insertions: [0], modifications: []))
        stub(episodeRepository) { stub in
            when(stub.getChanges(by: any(), filter: any())).thenReturn(.just(changes))
        }
        
        sut.didLoad()
        
        let result = try! sut.changeset.toBlocking().first()!
        expect(result.isEmpty).toEventuallyNot(beTrue(), timeout: 1)
    }
    
    func testItemsCount() {
        let changes = EntityChanges(items: episodes, changeset: Changeset(deletions: [], insertions: [0], modifications: []))
        stub(episodeRepository) { stub in
            when(stub.getChanges(by: any(), filter: any())).thenReturn(.just(changes))
        }
        
        sut.didLoad()
        
        let result = sut.itemsCount
        expect(result).to(equal(1))
    }
    
    func testItemAtIndex() {
        let changes = EntityChanges(items: episodes, changeset: Changeset(deletions: [], insertions: [0], modifications: []))
        stub(episodeRepository) { stub in
            when(stub.getChanges(by: any(), filter: any())).thenReturn(.just(changes))
        }
        
        sut.didLoad()
        
        let result = sut.item(at: 0)
        expect(result?.episode.name).to(equal(episodes.first?.name))
    }
    
}
