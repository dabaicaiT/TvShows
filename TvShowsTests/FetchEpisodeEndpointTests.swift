//
//  FetchScheduleEndpointTests.swift
//  TvShowsTests
//
//  Created by T on 2019-10-21.
//  Copyright © 2019 T. All rights reserved.
//

import XCTest
@testable import TvShows
import Nimble
import Alamofire

class FetchEpisodeEndpointTests: XCTestCase {
    
    func testInt() {
        let request = FetchEpisodeEndpoint.Request(date: "date")
        
        expect(request.headers).to(beNil())
        expect(request.path).to(equal("/schedule"))
        expect(request.httpMethod).to(equal(HTTPMethod.get))
        expect(request.parameters?["date"] as? String).to(equal("date"))
        expect(request.parameters?["country"] as? String).to(equal("US"))
    }
    
}
